'use strict';

/**
 * @ngdoc overview
 * @name webP4App
 * @description
 * # webP4App
 *
 * Main module of the application.
 */
angular
  .module('webP4App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
    .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'AccountsCtrl',
        controllerAs: 'accounts'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/accounts', {
        templateUrl: 'views/accounts.html',
        controller: 'AdminCtrl',
        controllerAs: 'admin'
      })
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs : 'Main'
      })

      .otherwise({
        redirectTo: '/'
      });
  });

  

