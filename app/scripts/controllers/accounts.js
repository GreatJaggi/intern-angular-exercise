'use strict';

/**
 * @ngdoc function
 * @name webP4App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webP4App
 */
angular.module('webP4App')
  .controller('AccountsCtrl', function ($scope, $http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
   	this.account = {};
    this.addAccount = function(){
    	console.log(this.account);
      $http.put('accounts.json', this.account, {headers: { 'Content-Type': 'application/json' }}).success(function(data){
        console.log("success");
      }).error(function(data){
        console.log(data);
      });
      this.account = {};
    };
  });