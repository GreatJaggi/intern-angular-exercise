'use strict';

/**
 * @ngdoc function
 * @name webP4App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the webP4App
 */
angular.module('webP4App')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
