'use strict';

/**
 * @ngdoc function
 * @name webP4App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webP4App
 */
angular.module('webP4App')
  .controller('MainCtrl', function($scope, $http) {
    var Users =this;
    $http.get('accounts.json').success(function(data){
      Users.users = data;
    });
    this.account = {};
    var something = 0;
    this.login = function(){
    	var username = this.account.username;
    	var pass = this.account.pass;
      Users.users.forEach(function(arrayItem){
        if(arrayItem.username === username && arrayItem.password === pass){
          something = 1;
          alert ('Logged In!');
        }
      });
      if(something === 0){
        alert('Login Error!');
      }
    };
 
});